libsys-virt-perl (11.0.0-1) unstable; urgency=medium

  * Import upstream version 11.0.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 11.0.0~)
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 12 Feb 2025 13:53:36 +0100

libsys-virt-perl (10.9.0-1) unstable; urgency=medium

  * Import upstream version 10.9.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 10.9.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 02 Nov 2024 20:53:39 +0100

libsys-virt-perl (10.6.0-1) unstable; urgency=medium

  * Import upstream version 10.6.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 10.6.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 09 Aug 2024 23:23:47 +0200

libsys-virt-perl (10.5.0-1) unstable; urgency=medium

  * Import upstream version 10.5.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 10.5.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 23 Jul 2024 06:56:10 +0200

libsys-virt-perl (10.2.0-1) unstable; urgency=medium

  * Import upstream version 10.2.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 10.2.0~)
  * Declare compliance with Debian policy 4.7.0

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 21 Apr 2024 22:06:27 +0200

libsys-virt-perl (10.1.0-1) unstable; urgency=medium

  * Import upstream version 10.1.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 10.1.0~)
  * Replace Build-Depends on pkg-config with pkgconf

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 06 Apr 2024 09:33:20 +0200

libsys-virt-perl (10.0.0-1) unstable; urgency=medium

  * Import upstream version 10.0.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 10.0.0~)
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 21 Jan 2024 22:03:46 +0100

libsys-virt-perl (9.8.0-1) unstable; urgency=medium

  * Import upstream version 9.8.0.

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 21 Nov 2023 07:27:48 +0100

libsys-virt-perl (9.7.0-2) unstable; urgency=medium

  [ Pino Toscano ]
  * Drop libvirt-daemon Build-Depends, no more needed now

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 03 Oct 2023 08:05:18 +0200

libsys-virt-perl (9.7.0-1) unstable; urgency=medium

  * Import upstream version 9.7.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 9.7.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 16 Sep 2023 16:12:24 +0200

libsys-virt-perl (9.4.0-1) unstable; urgency=medium

  * Import upstream version 9.4.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 9.4.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 04 Jul 2023 17:56:55 +0200

libsys-virt-perl (9.0.0-1) unstable; urgency=medium

  * Import upstream version 9.0.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 9.0.0~)
  * Declare compliance with Debian policy 4.6.2
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 01 Feb 2023 07:05:29 +0100

libsys-virt-perl (8.10.0-1) unstable; urgency=medium

  * Import upstream version 8.10.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 8.9.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 09 Dec 2022 20:59:29 +0100

libsys-virt-perl (8.9.0-1) unstable; urgency=medium

  * Import upstream version 8.8.0.
  * Drop "Fix spelling errors in manpage" (applied upstream)
  * Bump versioned Build-Depends on libvirt-dev to (>= 8.7.0~)
  * Import upstream version 8.9.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 8.9.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 20 Nov 2022 21:05:33 +0100

libsys-virt-perl (8.5.0-1) unstable; urgency=medium

  * Import upstream version 8.5.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 8.5.0~)
  * Update "Fix spelling errors in manpage" patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 09 Aug 2022 09:05:55 +0200

libsys-virt-perl (8.4.0-1) unstable; urgency=medium

  * Import upstream version 8.4.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 8.4.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 21 Jun 2022 22:03:57 +0200

libsys-virt-perl (8.3.0-1) unstable; urgency=medium

  * Import upstream version 8.3.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 8.3.0~)
  * Declare compliance with Debian policy 4.6.1

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 24 May 2022 13:18:26 +0200

libsys-virt-perl (8.1.0-1) unstable; urgency=medium

  * Import upstream version 8.1.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 8.1.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 20 Mar 2022 21:04:58 +0100

libsys-virt-perl (8.0.0-1) unstable; urgency=medium

  * Import upstream version 8.0.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 8.0.0~)
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 23 Jan 2022 14:52:45 +0100

libsys-virt-perl (7.10.0-1) unstable; urgency=medium

  * New upstream version 7.10.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 7.10.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 09 Dec 2021 12:22:19 +0100

libsys-virt-perl (7.9.0-1) unstable; urgency=medium

  * Import upstream version 7.7.0 and 7.9.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 7.9.0~)
  * debian/upstream/metadata: Drop use of Homepage field
  * Annotate test-only build dependencies with <!nocheck>

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 07 Dec 2021 07:28:57 +0100

libsys-virt-perl (7.5.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ Salvatore Bonaccorso ]
  * Import upstream version 7.5.0.
  * Bump versioned Build-Depends on libvirt-dev to (>= 7.5.0~)
  * Declare compliance with Debian policy 4.6.0

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 20 Aug 2021 22:58:15 +0200

libsys-virt-perl (7.0.0-1) unstable; urgency=medium

  * Import upstream version 6.10.0 and 7.0.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 7.0.0~)
  * Declare compliance with Debian policy 4.5.1
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 30 Jan 2021 09:56:36 +0100

libsys-virt-perl (6.8.0-1) unstable; urgency=medium

  * Import upstream version 6.8.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 6.8.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 30 Oct 2020 22:34:42 +0100

libsys-virt-perl (6.3.0-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Import upstream version 5.7.0, 5.8.0, 6.0.0, 6.1.0 and 6.3.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 6.3.0~)
  * Update copyright years for debian/* packaging files
  * debian/upstream/metadata: Update Repository URL for project

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.

  [ Salvatore Bonaccorso ]
  * Bump Debhelper compat level to 13
  * Declare compliance with Debian policy 4.5.0
  * debian/copyright: Correct syntax error from Files paragraph

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 13 Jul 2020 21:39:36 +0200

libsys-virt-perl (5.6.0-1) unstable; urgency=medium

  * Import upstream version 5.6.0
  * debian/copyright: Add stanza for new lib/Sys/Virt/NWFilterBinding.pm
  * Bump versioned Build-Depends on libvirt-dev to (>= 5.6.0~)
  * Declare compliance with Debian policy 4.4.0
  * debian/watch: Bump format to version 4.
    Use @ANY_VERSION@ and @ARCHIVE_EXT@ substitutions.
  * Bump Debhelper compat level to 12
  * debian/copyright: Add information for new lib/Sys/Virt/DomainCheckpoint.pm
  * Add patch to fix spelling errors in manpage

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 26 Aug 2019 20:14:29 +0200

libsys-virt-perl (5.0.0-1) unstable; urgency=medium

  * Import upstream version 5.0.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 5.0.0~)
  * Bump Debhelper compat level to 11
  * Declare compliance with Debian policy 4.3.0
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 22 Jan 2019 16:50:01 +0100

libsys-virt-perl (4.10.0-1) unstable; urgency=medium

  * Import upstream version 4.8.0 and 4.10.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 4.10.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 14 Dec 2018 21:09:45 +0100

libsys-virt-perl (4.7.0-1) unstable; urgency=medium

  * Import upstream version 4.7.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 4.7.0~)
  * Drop fix-more-spelling-error-in-manpage.patch
  * Declare compliance with Debian policy 4.2.1
  * Don't disable parallel building anymore

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 10 Sep 2018 21:03:07 +0200

libsys-virt-perl (4.6.0-1) unstable; urgency=medium

  * Import upstream version 4.6.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 4.6.0~)
  * Add new copyright stanza for lib/Sys/Virt/NWFilterBinding.pm
  * Declare compliance with Debian policy 4.2.0
  * debian/copyright: Correct file location for Virt.xs
  * Refresh fix-more-spelling-error-in-manpage.patch patch.
    Add one more spelling fix.

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 07 Aug 2018 16:03:19 +0200

libsys-virt-perl (4.5.0-1) unstable; urgency=medium

  * Import upstream version 4.4.0 and 4.5.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 4.5.0~)
  * Add Build-Depends on libmodule-build-perl
  * Declare compliance with Debian policy 4.1.5
  * Refresh fix-more-spelling-error-in-manpage.patch patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 18 Jul 2018 21:01:55 +0200

libsys-virt-perl (4.2.0-1) unstable; urgency=medium

  * Import upstream version 4.2.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 4.2.0~)
  * Declare compliance with Debian policy 4.1.4

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 07 Apr 2018 08:44:44 +0200

libsys-virt-perl (4.1.0-1) unstable; urgency=medium

  * Update Vcs-* headers for switch to salsa.debian.org
  * Import upstream version 4.1.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 4.1.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 15 Mar 2018 16:02:14 +0100

libsys-virt-perl (4.0.0-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Import upstream version 4.0.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 4.0.0~)
  * Refresh fix-more-spelling-error-in-manpage.patch patch
  * Bump Debhelper compat level to 10
  * Update copyright years for debian/* packaging files
  * Disable parallel building (cf. #888281)

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 24 Jan 2018 16:33:20 +0100

libsys-virt-perl (3.9.1-1) unstable; urgency=medium

  * Import upstream version 3.9.1
  * Set Rules-Requires-Root to no

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 05 Dec 2017 14:30:13 +0100

libsys-virt-perl (3.9.0-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.2 (no changes needed)

  [ Salvatore Bonaccorso ]
  * New upstream version 3.9.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 3.9.0~)
  * Refresh fix-more-spelling-error-in-manpage.patch patch (offsets)

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 04 Dec 2017 07:06:43 +0100

libsys-virt-perl (3.8.0-1) unstable; urgency=medium

  * Import upstream version 3.8.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 3.8.0~)
  * Declare compliance with Debian policy 4.1.1

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 12 Oct 2017 21:32:41 +0200

libsys-virt-perl (3.7.0-1) unstable; urgency=medium

  * Import upstream version 3.7.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 3.7.0~)
  * Declare compliance with Debian policy 4.1.0

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 14 Sep 2017 21:17:43 +0200

libsys-virt-perl (3.5.0-1) unstable; urgency=medium

  * Import upstream version 3.5.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 3.5.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 07 Jul 2017 20:26:54 +0200

libsys-virt-perl (3.4.0-1) unstable; urgency=medium

  * Import upstream version 3.1.0, 3.2.0 and 3.4.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 3.4.0~)
  * Declare compliance with Debian policy 4.0.0
  * Refresh fix-more-spelling-error-in-manpage.patch patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 06 Jul 2017 19:29:09 +0200

libsys-virt-perl (3.0.0-1) unstable; urgency=medium

  * Import upstream version 3.0.0
  * Update copyright years for upstream ./Virt.xs file
  * Update copyright years for debian/* packaging files
  * Bump versioned Build-Depends on libvirt-dev to (>= 3.0.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 25 Jan 2017 13:45:44 +0100

libsys-virt-perl (2.5.0-1) unstable; urgency=medium

  * Import upstream version 2.5.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 2.5.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 23 Dec 2016 10:43:56 +0100

libsys-virt-perl (2.4.0-1) unstable; urgency=medium

  * Import upstream version 2.4.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 2.4.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 03 Nov 2016 17:29:22 +0100

libsys-virt-perl (2.3.0-1) unstable; urgency=medium

  * Import upstream version 2.3.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 2.3.0~)
  * Refresh fix-more-spelling-error-in-manpage.patch patch.
    Add more spelling fixes as reported by lintian.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 08 Oct 2016 19:08:39 +0200

libsys-virt-perl (2.2.0-1) unstable; urgency=medium

  * Import upstream version 2.2.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 2.2.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 10 Sep 2016 08:13:34 +0200

libsys-virt-perl (2.1.0-1) unstable; urgency=medium

  * Import upstream version 2.1.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 2.1.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 08 Aug 2016 06:34:55 +0200

libsys-virt-perl (2.0.0-1) unstable; urgency=medium

  * Import upstream version 2.0.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 2.0.0~)
  * Drop one spelling error in manpage fixing patch (applied upstream)
  * Refresh fix-more-spelling-error-in-manpage.patch patch
  * Enable all hardening build flags

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 02 Jul 2016 07:24:50 +0200

libsys-virt-perl (1.3.5-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Import upstream version 1.3.5

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 06 Jun 2016 16:34:02 +0200

libsys-virt-perl (1.3.4-1) unstable; urgency=medium

  * Import upstream version 1.3.4
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.3.4~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 04 May 2016 12:51:14 +0200

libsys-virt-perl (1.3.3-1) unstable; urgency=medium

  * debian/control: Use HTTPS transport protocol for Vcs-Git URI
  * Import upstream version 1.3.2 and 1.3.3
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.3.3~)
  * Declare compliance with Debian policy 3.9.8
  * Fix spelling errors in various manpages

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 18 Apr 2016 17:55:41 +0200

libsys-virt-perl (1.3.1-1) unstable; urgency=low

  * Import upstream version 1.3.1
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.3.1~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 22 Jan 2016 19:47:51 +0100

libsys-virt-perl (1.3.0-2) unstable; urgency=low

  * Upload to unstable
  * Add fix-spelling-error-in-manpage.patch patch.
    Fix spelling errors in manpage found by lintian.
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 02 Jan 2016 13:16:47 +0100

libsys-virt-perl (1.3.0-1) experimental; urgency=low

  * Import upstream version 1.3.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.3.0~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 19 Dec 2015 19:03:34 +0100

libsys-virt-perl (1.2.21-1) unstable; urgency=low

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * Import upstream version 1.2.21
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.21~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 12 Nov 2015 06:29:12 +0100

libsys-virt-perl (1.2.19-1) unstable; urgency=low

  * Import upstream version 1.2.19
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.19~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 13 Sep 2015 19:31:37 +0200

libsys-virt-perl (1.2.18-1) unstable; urgency=low

  * Import upstream version 1.2.17 and 1.2.18
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.18~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 24 Aug 2015 07:22:23 +0200

libsys-virt-perl (1.2.16-1) unstable; urgency=low

  * Import upstream version 1.2.16
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.16~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 03 Jun 2015 17:43:31 +0200

libsys-virt-perl (1.2.15-1) unstable; urgency=low

  * Import upstream version 1.2.15
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.15~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 09 May 2015 06:55:26 +0200

libsys-virt-perl (1.2.14-1) unstable; urgency=low

  * Imported upstream version 1.2.12, 1.2.13 and 1.2.14
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.14~)
  * Update copyright for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 27 Apr 2015 18:53:02 +0200

libsys-virt-perl (1.2.9-2) unstable; urgency=medium

  * Change Build-Depends from libvirt-bin to libvirt-daemon
    libvirt-bin is now a transitional package and only libvirt-daemon is
    used during build.
  * Drop Depends on libvirt-bin.
    The runtime dependency to neither libvirt-daemon-system or
    libvirt-clients is required to have the Perl module working. It only
    requires the dependency on the library, since libvirt supports also
    accessing remote daemons via the network.
    Thanks to Daniel Gröber <dxld@darkboxed.org> (Closes: #774185)

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 30 Dec 2014 08:11:19 +0100

libsys-virt-perl (1.2.9-1) unstable; urgency=medium

  * Imported upstream version 1.2.9
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.9~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 17 Oct 2014 07:06:32 +0200

libsys-virt-perl (1.2.8-1) unstable; urgency=medium

  * Update Vcs-Browser URL to cgit web frontend
  * Add debian/upstream/metadata file
  * Imported upstream version 1.2.8
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.8~)
  * Update copyright years for debian/* packaging
  * Declare compliance with Debian Policy 3.9.6
  * Add Testsuite: autopkgtest-pkg-perl field in debian/control
  * Add debian/tests/pkg-perl/test-files copying additionally Changes file

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 23 Sep 2014 20:10:49 +0200

libsys-virt-perl (1.2.6-1) unstable; urgency=medium

  * Imported Upstream version 1.2.5 and 1.2.6
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.6~)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 08 Aug 2014 07:16:32 +0200

libsys-virt-perl (1.2.4-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 1.2.2, 1.2.3 and 1.2.4
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.4)

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 06 May 2014 21:51:26 +0200

libsys-virt-perl (1.2.1-1) unstable; urgency=medium

  * Imported Upstream version 1.2.1
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.1)
  * Update copyright years information for upstream files

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 21 Jan 2014 16:50:51 +0100

libsys-virt-perl (1.2.0-1) unstable; urgency=low

  * Imported Upstream version 1.2.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.2.0)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 20 Dec 2013 20:13:07 +0100

libsys-virt-perl (1.1.4-1) unstable; urgency=low

  * Imported Upstream version 1.1.3 and 1.1.4
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.1.4)
  * Bump Standards-Version to 3.9.5
  * Add Build-Depends on libvirt-bin
  * Add Depends on libvirt-bin.
    Add Depends on libvirt-bin as /usr/share/libvirt/cpu_map.xml is used.
  * Bump versioned Build-Depends on debhelper to (>= 9.20120312~)
    At least this version of debehlper is needed to get all hardening flags.
  * Wrap and sort fields in debian/control file

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 06 Nov 2013 17:19:29 +0100

libsys-virt-perl (1.1.2-1) unstable; urgency=low

  * Imported Upstream version 1.1.2
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.1.2)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 27 Sep 2013 22:45:16 +0200

libsys-virt-perl (1.1.1-1) unstable; urgency=low

  * Imported Upstream version 1.1.1
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.1.1)

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 07 Aug 2013 16:05:48 +0200

libsys-virt-perl (1.1.0-1) unstable; urgency=low

  * Imported Upstream version 1.1.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.1.0)

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 29 Jul 2013 18:39:05 +0200

libsys-virt-perl (1.0.5-1) unstable; urgency=low

  * Upload to unstable
  * Imported Upstream version 1.0.5
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.0.5)
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 29 May 2013 18:17:30 +0200

libsys-virt-perl (1.0.3-1) experimental; urgency=low

  * Imported Upstream version 1.0.3
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.0.3)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 08 Mar 2013 21:27:29 +0100

libsys-virt-perl (1.0.2-1) experimental; urgency=low

  * Imported Upstream version 1.0.2
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.0.2)

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 27 Feb 2013 20:53:19 +0100

libsys-virt-perl (1.0.1-1) experimental; urgency=low

  * Imported Upstream version 1.0.1
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.0.1)

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 26 Feb 2013 16:42:08 +0100

libsys-virt-perl (1.0.0-1) experimental; urgency=low

  * Imported Upstream version 1.0.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 1.0.0)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 16 Nov 2012 19:00:38 +0100

libsys-virt-perl (0.10.2-1) experimental; urgency=low

  * Imported Upstream version 0.10.2
  * Bump versioned Build-Depends on libvirt-dev to (>= 0.10.2)
  * Bump Standards-Version to 3.9.4

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 03 Oct 2012 11:09:32 +0200

libsys-virt-perl (0.10.0-1) experimental; urgency=low

  * Imported Upstream version 0.9.13 and 0.10.0
  * Bump versioned Build-Depends on libvirt-dev to (>= 0.10.0)

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 17 Sep 2012 22:17:17 +0200

libsys-virt-perl (0.9.12-2) unstable; urgency=low

  * Upload to unstable

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 15 Jun 2012 06:20:54 +0200

libsys-virt-perl (0.9.12-1) experimental; urgency=low

  * Imported Upstream version 0.9.12
  * Bump versioned Build-Depends on libvirt-dev to (>= 0.9.12)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 08 Jun 2012 07:50:53 +0200

libsys-virt-perl (0.9.11-1) unstable; urgency=low

  * Imported Upstream version 0.9.11
  * Upload package to unstable
  * Bump versioned Build-Depends on libvirt-dev to (>= 0.9.11)

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 16 Apr 2012 16:08:41 +0200

libsys-virt-perl (0.9.10-1) experimental; urgency=low

  * Imported Upstream version 0.9.10
  * Bump versioned Build-Depends on libvirt-dev to (>= 0.9.10)
  * Update debian/copyright file format.
    Update format to copyright-format 1.0 as released together with Debian
    policy 3.9.3.
  * Bump Standards-Version to 3.9.3

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 12 Mar 2012 07:25:28 +0100

libsys-virt-perl (0.9.9-1) unstable; urgency=low

  * Imported Upstream version 0.9.9
  * Bump versioned Build-Depends on libvirt-dev to (>= 0.9.9)
  * Add libcpan-changes-perl to Build-Depends
  * Drop fix-spelling-error-in-manpage.patch.
    Patch was applied upstream.
  * Bump Debhelper compat level to 9.
    Adjust Build-Depends on debhelper to (>= 9).

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 06 Feb 2012 22:13:39 +0100

libsys-virt-perl (0.9.8-1) unstable; urgency=low

  * Imported Upstream version 0.9.8
  * Bump versioned Build-Depends on libvirt-dev to (>= 0.9.8)
  * Refresh fix-spelling-error-in-manpage.patch patch
  * Refresh debian/copyright file.
    Update copyright years for debian/* packaging

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 03 Jan 2012 18:25:45 +0100

libsys-virt-perl (0.9.7-2) unstable; urgency=low

  * Upload package to unstable

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 22 Nov 2011 11:06:19 +0100

libsys-virt-perl (0.9.7-1) experimental; urgency=low

  * Imported Upstream version 0.9.7 (Closes: #647705)
  * Bump versioned Build-Depends on libvirt-dev to (>= 0.9.7).
  * Refresh fix-spelling-error-in-manpage.patch patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 11 Nov 2011 07:29:31 +0100

libsys-virt-perl (0.9.5-1) unstable; urgency=low

  * Imported Upstream version 0.9.5
  * Bump versioned Build-Depends on libvirt-dev to (>= 0.9.5).
  * Add fix-spelling-error-in-manpage.patch patch.
    Fixes spelling error in Sys::Virt::Domain manpage.

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 29 Sep 2011 08:01:45 +0200

libsys-virt-perl (0.9.4-1) unstable; urgency=low

  * Imported Upstream version 0.9.4
  * debian/control: Bump versioned Build-Depends on libvirt-dev to (>= 0.9.4).
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 29 Aug 2011 07:29:07 +0200

libsys-virt-perl (0.9.3-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * New upstream release
  * debian/control: Bump versioned Build-Depends on libvirt-dev to (>= 0.9.3).

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 29 Jul 2011 09:58:37 +0200

libsys-virt-perl (0.9.2-1) unstable; urgency=low

  * New upstream release
  * debian/control: Bump versioned Build-Depends on libvirt-dev to (>= 0.9.2).

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 11 Jul 2011 07:36:09 +0200

libsys-virt-perl (0.2.8-1) unstable; urgency=low

  * New upstream release
  * debian/control: Bump versioned Build-Depends on libvirt-dev to (>= 0.9.0).

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 30 Jun 2011 16:06:12 +0200

libsys-virt-perl (0.2.7-1) unstable; urgency=low

  * New upstream release
  * debian/control: Bump versioned Build-Depends on libvirt-dev to (>= 0.8.8).
  * debian/copyright: Update upstream copyright for added
    lib/Sys/Virt/Stream.pm.
  * Bump Standards-Version to 3.9.2 (no changes needed).

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 30 Jun 2011 12:12:51 +0200

libsys-virt-perl (0.2.6-1) unstable; urgency=low

  * New upstream release
  * Email change: Salvatore Bonaccorso -> carnil@debian.org
  * debian/control:
    - Bump versioned Build-Depends on libvirt-dev to (>= 0.8.7).
    - Make versioned Build-Depends on libtest-pod-perl and
      libtest-pod-coverage-perl unversioned.
  * debian/copyright:
    - Update copyright years for debian/* packaging.
    - Refer to Debian systems in general instead of only Debian GNU/Linux
      systems.
  * Bump Standards-Version to 3.9.1.

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 17 Feb 2011 07:36:28 +0100

libsys-virt-perl (0.2.4-1) unstable; urgency=low

  * New upstream release
  * Drop fix_spelling-error-in-manpage.patch patch which is applied upstream.
  * Bump Build-Depends on libvirt-dev to (>= 0.8.1).
  * Referesh debian/copyright for upstream files.
  * Bump Standards-Version to 3.8.4.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Thu, 20 May 2010 10:52:04 +0200

libsys-virt-perl (0.2.3-1) unstable; urgency=low

  * New upstream release
  * debian/control: Bump versioned Build-Depends on libvirt-dev (>= 0.7.5)
    since this is the current minimum required version of libvirt.
  * Refresh debian/copyright:
    - Update copyright years for debian/* packaging.
    - Add Stanza for lib/Sys/Virt/Secret.pm.
    - Update to revision 135 of DEP5 format secification for machine readable
      copyright file.
  * Convert to '3.0 (quilt)' package source format.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Sun, 17 Jan 2010 07:57:17 +0100

libsys-virt-perl (0.2.2-1) unstable; urgency=low

  * New upstream release
    - Add all new APIs upto libvirt 0.7.0 APIs
    - Add missing APIs and bugfixes.
  * debian/control: Bump versioned Build-Depends on libvirt-dev (>= 0.7.0)
    since this is the current minimum required version of libvirt.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Sun, 04 Oct 2009 07:38:17 +0200

libsys-virt-perl (0.2.1-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Changed: Replace versioned (build-)dependency on perl
      (>= 5.6.0-{12,16}) with an unversioned dependency on perl
      (as permitted by Debian Policy 3.8.3).
    - Bump Standards-Version to 3.8.3 (no changes needed).
    - Bump Build-Depends on libvirt-dev (>= 0.6.4)

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Wed, 26 Aug 2009 10:54:02 +0200

libsys-virt-perl (0.2.0-1) unstable; urgency=low

  * Initial Release. (Closes: #384576)

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Thu, 02 Jul 2009 19:45:52 +0200
